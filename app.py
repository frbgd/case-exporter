﻿import datetime
import json
import os

from configparser import ConfigParser

from thehive4py.api import TheHiveApi
import xlsxwriter


def log(msg):
    print(str(datetime.datetime.now()) + " " + msg)
    with open('app.log', 'a', encoding='utf-8') as file:
        file.write(str(datetime.datetime.now()) + " " + msg + '\n')


def get_conf():
    try:
        currentPath = os.path.dirname(os.path.abspath(__file__))
        cfg = ConfigParser()
        confPath = currentPath + '/app.conf'
        cfg.read(confPath)
        return cfg
    except:
        log('Error in get_conf()')
        raise


def search4cases(api, **filter):
    try:
        response = api.find_cases(**filter)

        if response.status_code == 200:
            return response.json()
        else:
            raise ValueError(json.dumps(response.json(), indent=4, sort_keys=True))
    except:
        log('Error in search4cases()')
        raise


def export_fields(cases, fields, custom_fields):
    try:
        cases_for_export = []
        for case in cases:
            case_for_export = {}
            for field in fields:
                if field not in case:
                    case_for_export[field] = ''
                else:
                    case_for_export[field] = case[field]
            for field in custom_fields:
                if field not in case['customFields']:
                    case_for_export[field] = ''
                else:
                    if 'string' in case['customFields'][field]:
                        case_for_export[field] = case['customFields'][field]['string']
                    elif 'date' in case['customFields'][field]:
                        case_for_export[field] = \
                            str(datetime.datetime.fromtimestamp(case['customFields'][field]['date'] / 1000))
                    elif 'number' in case['customFields'][field]:
                        case_for_export[field] = case['customFields'][field]['number']
                    elif 'boolean' in case['customFields'][field]:
                        case_for_export[field] = case['customFields'][field]['boolean']
                    else:
                        case_for_export[field] = ''
            cases_for_export.append(case_for_export)

        return cases_for_export
    except:
        log('Error in export_fields()')
        raise


def write_to_json(data, filename):
    try:
        with open('exports/' + filename + '.json', 'w', encoding='utf-8') as file:
            file.write(json.dumps(data, indent=4, ensure_ascii=False))
    except:
        log('Error in write_to_json()')
        raise


def write_to_excel(data, filename):
    try:
        rows = list(data)
        fields = dict(data[0]).keys()
        workbook = xlsxwriter.Workbook('exports/' + filename + '.xlsx')
        worksheet = workbook.add_worksheet()

        row_index = 0
        field_index = 0
        for field in fields:
            worksheet.write(row_index, field_index, field)
            field_index += 1
        row_index += 1
        for row in rows:
            field_index = 0
            for field in fields:
                worksheet.write(row_index, field_index, row[field])
                field_index += 1
            row_index += 1
        workbook.close()
    except:
        log('Error in write_to_excel()')
        raise


def main():
    try:
        log('Starting')

        if not os.path.exists('exports'):
            log('Creating directory \'exports\'')
            os.mkdir('exports')

        log('Reading config')
        cfg = get_conf()

        log('Initializing API')
        api = TheHiveApi \
            (cfg.get('TheHive', 'url'), cfg.get('TheHive', 'username'), password=cfg.get('TheHive', 'password'))

        log('Getting cases')
        case_filter = eval(cfg.get('ExportOptions', 'filter'))
        cases = search4cases(api, **case_filter)
        log(str(len(cases)) + ' cases have gotten')

        log('Exporting fields')
        fields = eval(cfg.get('ExportOptions', 'fields'))
        custom_fields = eval(cfg.get('ExportOptions', 'custom_fields'))
        cases = export_fields(cases, fields, custom_fields)

        filename = str(datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")) + '-case_export'
        log('Writing JSON')
        write_to_json(cases, filename)
        log('Writing XLSX')
        write_to_excel(cases, filename)

        log('Exiting')

    except Exception as e:
        log(str(e))
        log('Failing')


if __name__ == "__main__":
    main()
